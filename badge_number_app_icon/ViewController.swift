//
//  ViewController.swift
//  badge_number_app_icon
//
//  Created by Md Khaled Hasan Manna on 7/12/20.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(register(_ :)))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Send Badge", style: .done, target: self, action: #selector(sendBadge(_:)))
       
     
        
        
      
        
    }
    @objc  func register(_ sender : UIBarButtonItem){
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert,.badge,.sound]) { (success, error) in
            if(success){
                print("Successfully Registered")
            }
        }
     
        
        
    }
    @objc func sendBadge(_ sender : UIBarButtonItem){
        let application = UIApplication.shared
        let badgeNumber : Int = 2
        application.applicationIconBadgeNumber = badgeNumber
        application.registerForRemoteNotifications()
    }


}

